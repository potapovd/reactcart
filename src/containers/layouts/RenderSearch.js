import React, { Component } from 'react';
import {connect} from 'react-redux';

import {searchPhone} from '../../ac';


class RenderSearch extends Component {
    constructor(props){
        super(props)
        this.state = {
            value: ""
        }
    }

    render() {

        const handleChange = (event) => {
            console.log( event.target.value )
            this.setState({
               value: event.target.value 
            })
        }

        const handleSubmit = (event) => {
            //console.log("handleSubmit")
            event.preventDefault();
            this.props.searchPhone(this.state.value)
        }

        
        return (
            <div className="row">
                
               
                <div className="row">
                    <div className="col s12">
                        <h6>Quick Search</h6>
                    </div>
                    <form onSubmit = { handleSubmit }> 
                        <div className="input-field col s12">
                            <input 
                                id="search_field" 
                                type="text"
                                className="validate"
                                onChange = { handleChange }
                            />
                            <label htmlFor="search_field">ex. Apple</label>
                        </div>
                        <div className="col s12">
                            <button className="waves-effect waves-light btn">
                                <i className="material-icons left">search</i> Search
                            </button>
                        </div>
                    </form>
                    
                </div>
            </div>
        );
        
        
    }

    
}

const mapDispatchToProps = {
    searchPhone
}

export default connect(null, mapDispatchToProps)(RenderSearch);
