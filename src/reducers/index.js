import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import phones from './phones';
import phonesPage from './phonesPage';
import phonePage from './phonePage';
import basket from './basket';

export default combineReducers({
    routing,
    phones,
    phonesPage,
    phonePage,
    basket
})

