import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    getTotalBasketCount,
    getTotalBasketPrice
} from '../../selectors';

const RenderBasketCart = ({totalBasketCount,totalPrice}) => (
    <ul className="right ">
        <li>
            <Link 
                to='basket' 
                id="dLabel" 
                className="waves-effect waves-light blue darken-2 btn"
            >
                { totalBasketCount ? `${totalBasketCount} item(s) - $${totalPrice}` : "Cart" }
                <i className="material-icons left">shopping_cart</i>
            </Link>
        </li>
    </ul>
)

const mapStateToProps = (state)=>({
    totalBasketCount: getTotalBasketCount(state),
    totalPrice: getTotalBasketPrice(state) 

})

export default connect(mapStateToProps,null)(RenderBasketCart);