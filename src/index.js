//libs
import React from 'react';
import ReactDOM from 'react-dom';
//Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers';
//Routing-4
import { Router, Route,  Switch } from 'react-router';
import { createBrowserHistory } from 'history';
//Layouts
import Phones from './containers/phones';
import Phone from './containers/phone';
//import './main.css';

const store = createStore(reducers, composeWithDevTools(
    applyMiddleware(thunk)
));
const history = createBrowserHistory();

ReactDOM.render(
    <Provider store = { store } key = "provider">
      <Router history = { history } >
        <Switch>
            <Route exact path = "/" component = { Phones } />
            <Route  path = "/phone/:id" component = { Phone } />  
        </Switch>
      </Router>  
    </Provider>,
    document.getElementById('root')
);