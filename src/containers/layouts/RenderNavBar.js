import React from 'react';
import {Link} from 'react-router-dom';
import RenderBasketCart from './RenderBasketCart';

const RenderNavBar = () => (
    <div className="navbar-fixed">
        <nav>
            <div className="nav-wrapper">
                <Link to="/" className="brand-logo center">
                    ReactCart
                </Link>
                <RenderBasketCart />
            </div>
        </nav>
    </div>
)

export default RenderNavBar;