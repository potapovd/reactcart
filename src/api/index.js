import * as R from 'ramda';
import phones from './fakeApiPhones';

export const fetchPhones = async () => {
    return new Promise ((resolve)=>{
        resolve(phones)
        //reject("404")
    })
}

export const loadMorePhones = async ({offset}) => {
    return new Promise ((resolve)=>{
        resolve(phones)
        //reject("404")
    })
}


export const fetchPhoneById = async (id) => {
    return new Promise ((resolve, reject)=>{
        const phone = R.find(R.propEq('id',id), phones)
        resolve (phone);
    })
}