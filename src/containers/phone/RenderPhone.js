import React from 'react';
import { Link } from 'react-router-dom';
import RenderTable from './RenderTable';
import { connect } from 'react-redux';
import { addPhoneToBasket } from '../../ac'

const RenderPhone = (props) =>{
    const {
        id,
        name,
        image,
        description,
        price
    } = props.phone
    return(
        <div className="col s12">
            <h2 className="header">{ name }</h2>
            <div className="card horizontal">
                <div className="card-image">
                    <img src={ image } alt="" />
                </div>
                <div className="card-stacked">
                
                    <div className="card-content">
                        <Link to="/" className="waves-effect waves-light teal darken-2 btn-small">
                            <i className="material-icons left">chevron_left</i>Back to store
                        </Link>
                        <p>{ description }</p>
                        <RenderTable phone={props} />
                        <h5 className="right-align">Price: <b>{ price }$</b></h5>
                    </div>
                    <div className="card-action">
                       <button 
                            className="waves-effect waves-light btn blue darken-2"
                            onClick = {()=>props.addPhoneToBasket(id)}
                        >
                            <i className="material-icons left">add_shopping_cart</i>
                            Add to cart
                        </button> 
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapDispatchToProps = {
    addPhoneToBasket
}

export default connect(null, mapDispatchToProps)(RenderPhone);

