import React from 'react';
import * as R from 'ramda';

const RenderTable = (props) => {
    const { phone } = props.phone;
    const fields = R.compose(
        R.toPairs,
        R.pick([
            'cpu',
            'camera',
            'display',
            'memory',
            'battery'
        ])
    )(phone)

    //console.log(props.phone)

    const tableBody = fields.map(([key, value], index)=>(
        <tr key={index}>
            <td><b>{key}</b></td>
            <td>{value}</td>
        </tr>
    ))

    //console.log(fields)

    return (
        <table className="highlight">
            <tbody>
                {tableBody}
            </tbody>
         </table>
    )
}

export default RenderTable;