import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import * as R from 'ramda';
import { connect } from 'react-redux';

import RenderNavBar from '../layouts/RenderNavBar';
import RenderSearch from '../layouts/RenderSearch';
import { 
    fetchPhones, 
    loadMorePhones, 
    addPhoneToBasket 
} from '../../ac';

import { getPhones } from '../../selectors';


class Phones extends Component {

    componentDidMount(){
        this.props.fetchPhones()
    }

    render() {
        const {
            phones, 
            loadMorePhones,
            addPhoneToBasket
        } = this.props;

        let renderAllPhones = <h2 className="center-align">Phones not found</h2>;
        let loadMoreButton = "";
        if( R.length(phones)>0 ){
            renderAllPhones = phones.map((phone, index)=>(
                <div className="col s12 m6" key={index}>
                    <div className="card">
                        <div className="card-image">
                            <img src={phone.image} alt="" />
                            <button 
                                className="btn-floating halfway-fab btn waves-effect waves-light blue darken-2" 
                                onClick = {()=>addPhoneToBasket(phone.id)}
                            >
                                <i className="material-icons">add_shopping_cart</i>
                            </button>
                        </div>
                        <div className="card-content">
                        <span className="card-title">{phone.name}</span>
                            <p>{R.take(60,phone.description)}</p> 
                        </div>
                        <div className="card-action">
                            <Link to={`phone/${phone.id}`}>
                                Details
                            </Link>
                        </div>
                    </div>
                </div>
            ));

            loadMoreButton = 
            <div className="row">
                <div className="col s12">
                    <div className="center-align">
                        <button 
                            className="btn-large waves-effect waves-light teal darken-2"
                            onClick = {loadMorePhones}
                        >
                            Load more
                            <i className="material-icons right">cached</i>
                        </button>
                    </div>
                </div>
            </div>
        }

        

        return (

        <div>
            <RenderNavBar />
            <div className="container">
                <div className="row">
                    <div className="col m3 s12">
                        <RenderSearch />
                    </div> 
                    <div className="col m9 s12">
                        <div className = "row">
                            { renderAllPhones ? renderAllPhones : null}
                        </div>
                        {loadMoreButton}
                    </div>
                </div>
            </div>
        </div>
           
        );
    }
}

const mapStateToProps = state => ({
    phones: getPhones(state) 
})

const mapDispatchToPhones = {
    fetchPhones,
    loadMorePhones,
    addPhoneToBasket
}
export default connect(mapStateToProps, mapDispatchToPhones)(Phones)
