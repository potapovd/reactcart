import React, { Component } from 'react';
import { matchPath } from 'react-router';
import { connect } from 'react-redux';

import RenderNavBar from '../layouts/RenderNavBar';
import RenderPhone from './RenderPhone';
import { fetchPhoneById } from '../../ac';
import { getPhoneById } from '../../selectors';



class Phone extends Component {
   
    componentDidMount(){
        const match = matchPath(this.props.history.location.pathname, {
            path: '/phone/:id',
            exact: true,
            strict: false
          })
        const id = match.params.id;
        this.props.fetchPhoneById( id );
    }
    
    render() {        
        const { phone } = this.props
        return (
        <div>
            <RenderNavBar />
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <div className="row">
                            {phone ? (
                                <RenderPhone phone = { phone } />
                            ): null}
                        </div>
                    </div> 
                </div>
            </div>
        </div>
            
        );
    }
}

const mapStateToProps = state => ({
    phone: getPhoneById(state, state.phonePage.id)
}) 

const mapDispatchToProps = {
    fetchPhoneById
}

export default connect(mapStateToProps, mapDispatchToProps)(Phone);